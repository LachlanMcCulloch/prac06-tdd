package movieListExercise2;

import movieListQuestions.MovieListException;

public class Thumbs implements Rating {
	private String thumbs;
	public Thumbs(int rating) throws MovieListException {
		if (rating < -2 || rating > 2 ){
			throw new MovieListException("Number of thumbs out of range <-2 to 2>");
		} else {
			switch (rating){
			case -2: thumbs = "Two thumbs down";
			break;
			case -1: thumbs = "One thumb down";
			break;
			case 1: thumbs = "One thumb up";
			break;
			case 2: thumbs = "Two thumbs up";
			break;
			}
		}
	}

	@Override
	public String getRating() {
		return thumbs;
	}

}
