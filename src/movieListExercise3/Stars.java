package movieListExercise3;

import movieListQuestions.MovieListException;

public class Stars implements Rating {
	private String stars; 
	public Stars(int rating) throws MovieListException {
		if (rating < 1 || rating > 5){
			throw new MovieListException("Number of Stars out of range <1-5>");
		} else {
			stars = "";
			for (int i=0; i<rating; i++){
					stars += "*";
			}
		}
	}

	@Override
	public String getRating() {
		return stars;
	}

}
