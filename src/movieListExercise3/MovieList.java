package movieListExercise3;

import java.util.Map;
import java.util.TreeMap;

import movieListQuestions.MovieListException;
import movieListExercise3.oscarDB;

public class MovieList {
	private Map<String, String> movieList;
	/**
	 * Constructor of MovieList
	 */
	public MovieList() {
		movieList = new TreeMap<String, String>();
	}

	public void addMovie(String movieName) throws MovieListException {
		if (movieList.containsKey(movieName)){
			throw new MovieListException("Duplicate movie: " + movieName);
		}
		movieList.put(movieName, "No rating");
		
	}

	public Object getRating(String movieName) throws MovieListException {
		if (movieList.containsKey(movieName)){
			return movieList.get(movieName);
		} else {
			throw new MovieListException("Unknown movie: " + movieName);
		}
	}

	public void setRating(String movieName, Rating opinion) throws MovieListException {
		if (!movieList.containsKey(movieName)){
			throw new MovieListException("Movie does not exist: " + movieName);
		}
		movieList.put(movieName, opinion.getRating());
		
	}

	public String getList() {
		String list = ""; 
		for (String movieName : movieList.keySet()){
			if (oscarDB.oscarWinner(movieName)){
				list += "Oscar winner: ";
			}
			list += movieName + "\n";
		}
		return list;
	}
}
