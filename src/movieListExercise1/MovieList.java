package movieListExercise1;

import java.util.Map;
import java.util.TreeMap;

import movieListQuestions.MovieListException;

public class MovieList {
	private Map<String, String> movieList;
	/**
	 * Constructor of MovieList
	 */
	public MovieList() {
		movieList = new TreeMap<String, String>();
	}

	public void addMovie(String movieName) throws MovieListException {
		if (movieList.containsKey(movieName)){
			throw new MovieListException("Duplicate movie: " + movieName);
		}
		movieList.put(movieName, "No rating");
		
	}

	public Object getRating(String movieName) throws MovieListException {
		if (movieList.containsKey(movieName)){
			return movieList.get(movieName);
		} else {
			throw new MovieListException("Unknown movie: " + movieName);
		}
	}

	public void setRating(String movieName, int rating) throws MovieListException {
		if (rating < 1 || rating > 5){
			throw new MovieListException("Number of Stars out of range <1-5>");
		}
		if (!movieList.containsKey(movieName)){
			throw new MovieListException("Movie does not exist: " + movieName);
		}
		String ratingStr = "";
		for (int i=0; i<rating; i++){
			ratingStr += "*";
		}
		movieList.put(movieName, ratingStr);
		
	}

	public String getList() {
		String list = ""; 
		for (String movieName : movieList.keySet()){
			list += movieName + "\n";
		}
		return list;
	}
}
